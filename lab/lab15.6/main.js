// 15.6.1

// du lieu 1
var massMark1 = 78;
var heightMark1 = 1.69;
var massJohn1 = 92;
var heightJohn1 = 1.95;

var bmiMark1 = massMark1/(heightMark1 * heightMark1)
console.log('bmiMark1 la ' + bmiMark1)

var bmiJohn1 = massJohn1/(heightJohn1 * heightJohn1)
console.log('bmiJohn1 la ' + bmiJohn1)

var markHigherBMI1 = bmiMark1 > bmiJohn1;
if (markHigherBMI1 == true){
    console.log('BMI cua Mark cao hon BMI cua John')
}else {
    console.log('BMI cua Mark thap hon BMI cua John ')
}


// du lieu 2
var massMark2 = 95;
var heightMark2 = 1.88;
var massJohn2 = 85;
var heightJohn2 = 1.76;

var bmiMark2 = massMark2/(heightMark2 * heightMark2)
console.log('bmiMark2 la ' + bmiMark2)

var bmiJohn2 = massJohn2/(heightJohn2 * heightJohn2)
console.log('bmiJohn2 la ' + bmiJohn2)

var markHigherBMI2 = bmiMark2 > bmiJohn2;
if (markHigherBMI2 == true){
    console.log('BMI cua Mark cao hon BMI cua John')
}else {
    console.log('BMI cua Mark thap hon BMI cua John ')
}


// 15.6.2

// du lieu 1
if (bmiMark1 > bmiJohn1){
    console.log("Mark's BMI is higher than John's!")
}else{
    console.log("John's BMI is higher than Mark's!")
}

// du lieu 2
if (bmiJohn2 > bmiMark2){
    console.log("John's BMI is higher than Mark's!")
}else{
    console.log("Mark's BMI is higher than John's!")
}



// du lieu 1
if (bmiMark1 > bmiJohn1){
    console.log(`Mark's BMI (27.3) is higher than John's (24.1)!`)
}else{
    console.log(`John's BMI (24.1) is higher than Mark's (27.3)!`)
}

// du lieu 2
if (bmiJohn2 > bmiMark2){
    console.log(`John's BMI (24.1) is higher than Mark's (27.3)!`)
}else{
    console.log(`Mark's BMI (27.3) is higher than John's (24.1)!`)
}